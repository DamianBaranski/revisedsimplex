#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "table.h"
#include <fstream>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Eigen::MatrixXf A1, A2, B1, B2, C;
    int n,m1,m2;
    std::fstream file;

private slots:

    void on_nBox_valueChanged(int arg1);

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_m1Box_valueChanged(int arg1);

    void on_m2Box_valueChanged(int arg1);

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

private:
    void update_matrix();
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
