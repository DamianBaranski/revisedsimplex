#ifndef TABELASIMPLEX_H
#define TABELASIMPLEX_H
#include <QTableWidget>
#include <eigen3/Eigen/Eigen>
#include <vector>
#include <gnuplot.h>

class point
{
public:
    double x,y;
    point(){x=0;y=0;}
    point(double X){x=X;y=0;}
    point(double X,double Y){x=X;y=Y;}
    bool operator < (const point second) const {/*if(this->x==second.x) return this->y < second.y; else*/ return this->x < second.x; }
};


class TabelaSimplex : public QTableWidget
{
public:
    int test();
    int plot();

    TabelaSimplex();
    TabelaSimplex(QWidget *parent=0);
    ~TabelaSimplex();
    void SetMatix(Eigen::MatrixXf A1, Eigen::MatrixXf A2, Eigen::MatrixXf B1, Eigen::MatrixXf B2, Eigen::MatrixXf C);
    std::string textStatus;
    std::string longText;

private:
    void build_table();
    void update();
    int step;
    QTableWidgetItem* CiCx;
    QTableWidgetItem* Bi_name;
    QTableWidgetItem* BiAk;
    QTableWidgetItem* dash1;
    QTableWidgetItem* dash2;
    QTableWidgetItem* XiXj;
    QTableWidgetItem* ZjCj_name;
    QTableWidgetItem* Zj_name;
    QTableWidgetItem* Zx;
    std::vector<QTableWidgetItem*> Ci;
    std::vector<QTableWidgetItem*> Cj;
    std::vector<QTableWidgetItem*> Bi;

    std::vector<QTableWidgetItem*> BiAi;
    std::vector<QTableWidgetItem*> Zj;
    std::vector<QTableWidgetItem*> ZjCj;

    std::vector<QTableWidgetItem*> Xi_names;
    std::vector<QTableWidgetItem*> Xj_names;

    std::vector<int> bv;
    std::vector<int> first_bv;
    std::vector<int> nv;

    std::vector<std::vector<QTableWidgetItem*> > A_matrix;


    Eigen::MatrixXf C;
    Eigen::MatrixXf A;
    Eigen::MatrixXf B;

    Eigen::MatrixXf BA;
    Eigen::MatrixXf Z;
    Eigen::MatrixXf CZ;

    Eigen::MatrixXf pi;
    Eigen::MatrixXf C_hat;
    Eigen::MatrixXf out_con;
    int n;
    int m;
    int artificial_var_count;
    int mw;  //Moj warunek

    int status;


    //Potrzebe do wykresu
    Eigen::MatrixXf A1;
    Eigen::MatrixXf A2;
    Eigen::MatrixXf B1;
    Eigen::MatrixXf B2;
    int m1,m2,n_w;
    double z;
    std::vector<point> kolejne_rozwiazania;
    gnuplot wykres;

};

#endif // TABELASIMPLEX_H
