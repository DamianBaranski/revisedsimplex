#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QFileDialog>





MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->A1->setMatrix(&A1);
    ui->A2->setMatrix(&A2);
    ui->B1->setMatrix(&B1);
    ui->B2->setMatrix(&B2);
    ui->C->setMatrix(&C);

    n=0; m1=0; m2=0;
    on_nBox_valueChanged(2);
    on_m1Box_valueChanged(2);
    on_m2Box_valueChanged(2);

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::update_matrix()
{

    C.resize(1,n);
    C.setZero(1,n);

    A1.resize(m1,n);
    A1.setZero(m1,n);

    B1.resize(m1,1);
    B1.setZero(m1,1);

    A2.resize(m2,n);
    A2.setZero(m2,n);

    B2.resize(m2,1);
    B2.setZero(m2,1);

    ui->A1->update();
    ui->A2->update();
    ui->B1->update();
    ui->B2->update();
    ui->C->update();

}

void MainWindow::on_pushButton_2_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,
                                                    tr("Zapisz dane"), "",
                                                    tr("Dane (*.txt);;All Files (*)"));

    file.open(fileName.toStdString().c_str(), std::ios::out | std::ios::trunc);
    if(file.is_open())
    {
        file << n << " " << m1 << " " << m2 << "\n\n";
        ui->A1->save(&file);
        ui->B1->save(&file);
        ui->A2->save(&file);
        ui->B2->save(&file);
        ui->C->save(&file);

        file.close();
        ui->statusBar->showMessage(tr("Plik został zapisany"));

    }
    else
    {
        ui->statusBar->showMessage(tr("Zapis nie powiódł się!"));
    }

}

void MainWindow::on_pushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Otwórz dane"), "",
                                                    tr("Dane (*.txt);;All Files (*)"));
    file.open(fileName.toStdString().c_str());
    if(file.is_open())
    {
        file >> n >> m1 >> m2;
        ui->nBox->setValue(n);
        ui->m1Box->setValue(m1);
        ui->m2Box->setValue(m2);

        ui->A1->open(&file,m1,n);
        ui->B1->open(&file,m1,1);
        ui->A2->open(&file,m2,n);
        ui->B2->open(&file,m2,1);
        ui->C->open(&file,1,n);
        file.close();
        ui->statusBar->showMessage(tr("Plik został otwarty"));
        //Buduje tablice simplex
        ui->TS->SetMatix(A1,A2,B1,B2,C);
        //Zmienniam zakładkę
        ui->tabWidget->setCurrentIndex(1);
        QPalette kolor;
        kolor.setColor(QPalette::WindowText, Qt::black);
        ui->label_12->setText("Status");
        ui->label_12->setPalette(kolor);
        if(n==2)
            ui->pushButton_6->setEnabled(1);
        else
            ui->pushButton_6->setEnabled(0);
        ui->pushButton_5->setEnabled(1);
        ui->pushButton_4->setEnabled(1);


    }
    else
    {
        ui->statusBar->showMessage(tr("Plik nie może zostać otwarty"));
        QPalette kolor;
        kolor.setColor(QPalette::WindowText, Qt::black);
        ui->label_12->setText("Status");
        ui->label_12->setPalette(kolor);
    }
}

void MainWindow::on_m1Box_valueChanged(int arg1)
{
    m1=arg1;
    m2=std::min(m2,5-m1);
    ui->m2Box->setValue(m2);
    update_matrix();
}


void MainWindow::on_nBox_valueChanged(int arg1)
{
    n=arg1;
    update_matrix();
}

void MainWindow::on_m2Box_valueChanged(int arg1)
{
    m2=arg1;
    m1=std::min(m1,5-m2);
    ui->m1Box->setValue(m1);
    update_matrix();
}

void MainWindow::on_pushButton_3_clicked()
{
    QPalette kolor;
    kolor.setColor(QPalette::WindowText, Qt::black);
    ui->label_12->setText("Status");
    ui->label_12->setPalette(kolor);

    ui->TS->SetMatix(A1,A2,B1,B2,C);
    if(n==2)
        ui->pushButton_6->setEnabled(1);
    else
        ui->pushButton_6->setEnabled(0);
    ui->pushButton_5->setEnabled(1);
    ui->pushButton_4->setEnabled(1);

}

void MainWindow::on_pushButton_4_clicked()
{

    QPalette kolor;
    QString statusName[]={"W trakcie","Jedno rozwiązanie","Brak rozwiązań","Problem nieograniczony","Zapętlenie","Wiele rozwiazan","Rozwiazanie zdegenerowane","Zbior nieograniczony"};
    int status=ui->TS->test();
    qDebug() <<"Status:" << status;
    if(status==5 ||  status==7)
        ui->label_12->setText(ui->TS->textStatus.c_str());
    else
        ui->label_12->setText(statusName[status]);
    if(status==0)
        kolor.setColor(QPalette::WindowText, Qt::black);
    else if(status==1 || status==5)
        kolor.setColor(QPalette::WindowText,QColor(0,127,0));
    else if(status>1)
        kolor.setColor(QPalette::WindowText,QColor(127,127,0));
    ui->label_12->setPalette(kolor);
    ui->textEdit->setText(ui->TS->longText.c_str());


}

void MainWindow::on_pushButton_5_clicked()
{
    QPalette kolor;
    QString statusName[]={"W trakcie","Jedno rozwiązanie","Brak rozwiązań","Problem nieograniczony","Zapętlenie","Wiele rozwiazan","Rozwiazanie zdegenerowane","Zbior nieograniczony"};
    int status=0;

    while(status==0)
    {
        status=ui->TS->test();
        qDebug() <<"Status:" << status;
        if(status==5 ||  status==7)
            ui->label_12->setText(ui->TS->textStatus.c_str());
        else
            ui->label_12->setText(statusName[status]);
        if(status==0)
            kolor.setColor(QPalette::WindowText, Qt::black);
        else if(status==1 || status==5)
            kolor.setColor(QPalette::WindowText,QColor(0,127,0));
        else if(status>1)
            kolor.setColor(QPalette::WindowText,QColor(127,127,0));


        ui->label_12->setPalette(kolor);
        ui->textEdit->setText(ui->TS->longText.c_str());

    }

}

void MainWindow::on_pushButton_6_clicked()
{
    ui->TS->plot();

}
