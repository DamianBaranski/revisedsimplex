#include "table.h"
#include <QDebug>

template <typename T> std::string tostr(const T& t) {
   std::ostringstream os;
   os<<t;
   return os.str();
}

table::table()
{

}

table::table(QWidget *parent)
    :QTableView()
{
    setModel(&model);
}

void table::update()
{
   if(Matrix==NULL)
       return;
   n=Matrix->rows();
   m=Matrix->cols();


   model.setColumnCount(m);
   model.setRowCount(n);

   for(int i=0; i<n; i++)
   {
       for(int j=0; j<m; j++)
       {
           model.setData(model.index(i,j),(*Matrix)(i,j));
       }
   }

   for(int i=0; i<m; i++)
      this->setColumnWidth(i,35);

}

std::string table::save(std::fstream *file)
{
  std::string tmp;
  //tmp+=tostr(m)+"  "+tostr(n)+"\n";
  for(int i=0; i<n; i++)
  {
      for(int j=0; j<m; j++)
      {
          tmp+=tostr((*Matrix)(i,j));
          tmp+="  ";
      }
      tmp+="\n";
  }
  tmp+="\n";
  if(file->is_open())
      (*file) << tmp;
  qDebug() << tmp.c_str();
  return tmp;
}

void table::open(std::fstream *file, int rows, int cols)
{
    float tmp;
    if(file->is_open())
    {
        m=cols; n=rows;
        Matrix->resize(n,m);
        for(int i=0; i<n; i++)
        {
            for(int j=0; j<m; j++)
            {
                (*file) >> tmp;
                (*Matrix)(i,j)=tmp;

            }
        }
        update();
    }
}

void table::setMatrix(Eigen::MatrixXf *matrix)
{
  Matrix=matrix;
  update();
}

table::~table()
{

}

void table::closeEditor( QWidget * editor, QAbstractItemDelegate::EndEditHint hint )
{
    for(int i=0; i<n; i++)
    {
        for(int j=0; j<m; j++)
        {
            (*Matrix)(i,j)=model.data(model.index(i,j)).toDouble();
        }
    }
    QTableView::closeEditor(editor, hint);
}

