#-------------------------------------------------
#
# Project created by QtCreator 2015-05-18T17:47:30
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = simpleks
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    table.cpp \
    tabelasimplex.cpp \
    gnuplot.cpp

HEADERS  += mainwindow.h \
    table.h \
    tabelasimplex.h \
    gnuplot.h

FORMS    += mainwindow.ui
