#include "tabelasimplex.h"
#include <QDebug>
#include <iostream>
#include <algorithm>
#include <iostream>
#include <algorithm>

#include <fstream>

int det_matrix(point x,point y,point z)
{
    return(x.x*y.y+y.x*z.y+z.x*x.y-z.x*y.y-x.x*z.y-y.x*x.y);
}
bool check_intersection(point x, point y, point z, point w)
{
    if ((det_matrix(x,y,z))*(det_matrix(x,y,w))>=0)
        return 0; //Odcinki sie NIE przecinaja
    else
        if ((det_matrix(z,w,x))*(det_matrix(z,w,y))>=0)
            return 0; //Odcinki sie NIE przecinaja
        else //znaki wyznaczników sa równe
            return 1; //Odcinki sie przecinaja
}

int check_intersection2(std::vector<point> pkt_ok)
{
    for(int i=0; i<pkt_ok.size()-1;i++)
    {
        for(int j=0; j<pkt_ok.size()-1;j++)
        {
            if(check_intersection(pkt_ok[i],pkt_ok[i+1],pkt_ok[j],pkt_ok[j+1]))
            {
                return i;
                qDebug() << "Przecina się !!!";
            }

        }
        if(check_intersection(pkt_ok[i],pkt_ok[i+1],pkt_ok[0],pkt_ok[pkt_ok.size()-1]))
        {
            return i;
            qDebug() << "Przecina się !!!";
        }

    }
    return -1;
}

bool sort_index ( std::pair<int,int> left, std::pair<int,int> right)
{
    return left.first < right.first;
}

int TabelaSimplex::test()
{
    longText="";
    for(int i=0;i<m;i++)
    {
        qDebug() << "Bv[" << i << "]=" << bv[i];
    }
    for(int i=0;i<n-m;i++)
    {
        qDebug() << "Nv[" << i << "]=" << nv[i];
    }

    if(step>50)
    {
        //Zapetlenie
        return 4;
    }

    //qDebug() << "n " << n ;
    //qDebug() << "m " << m ;

    //Macierz bazowa A
    Eigen::MatrixXf AB;
    AB.resize(m,m);

    //Macierz bazowa B
    Eigen::MatrixXf CB;
    CB.resize(1,m);

    //Wpisuje odpowiednie kolumny do macierzy bazwych A,B
    for(int i=0;i<m;i++)
    {
        AB.col(i) << A.col(bv[i]);
        CB.col(i) << C.col(bv[i]);
    }

    //Macierz niebazowa A
    Eigen::MatrixXf AI;
    AI.resize(m,n-m);

    //Macierz niebazowa C
    Eigen::MatrixXf CI;
    CI.resize(1,n-m);

    std::vector<int> temp_nv=nv;
    std::sort(temp_nv.begin(),temp_nv.end());
    //Wpisuje odpowiednie kolumny do macierzy niebazwych A,B
    for(int i=0;i<n-m;i++)
    {
        AI.col(i) << A.col(nv[i]);
        CI.col(i) << C.col(nv[i]);
        //    qDebug() << "i=" << i;
    }


    pi=CB*AB.inverse();

    C_hat=CI-pi*AI;
    std::cout << "AB:" << std::endl << AB <<std::endl;
    std::cout << "CB:" << std::endl << CB <<std::endl;
    std::cout << "AI:" << std::endl << AI <<std::endl;
    std::cout << "CI:" << std::endl << CI <<std::endl;
    std::cout << "C_hat:" << std::endl << C_hat <<std::endl;

    Eigen::MatrixXf::Index maxRow, maxCol;

    //Szukamy max w chat
    float max = C_hat.maxCoeff(&maxRow, &maxCol);
    mw=maxCol;
    //Implementacja blanta
    for(int i=0;i<n-m;i++)
    {
        std::vector<int>::iterator it;
        it=std::find(nv.begin(),nv.end(),temp_nv[i]);
        //  qDebug() << "po kolei: " << nv[int(it-nv.begin())]+1 << "  "  << C_hat(0,int(it-nv.begin()));
        if(C_hat(0,int(it-nv.begin()))>0)
        {
            maxCol=int(it-nv.begin());
            break;
        }
    }



    Eigen::MatrixXf b_hat=AB.inverse()*B;




    //Wyswietlam wynik do warstwic
    z=(pi*B)(0,0);
    //std::cout << "Wynik=" << pi*B << std::endl;
    Zx->setText(QString("Z(x)=%1").arg((double)(pi*B)(0,0)));
    /*  for(int i=0; i<n;i++)
    {
        std::cout << "x" << i << "\t";
    }*/
    // std::cout << std::endl;

    //uzupełniam CZ(wektor rozwiazan) ze zmiennych bazowych a reszte uzupełniam zerami
    for(int i=0; i<n;i++)
    {
        int ok=1;
        for(int ii=0;ii<m;ii++)
        {
            if(bv[ii]==i)
            {
                CZ(0,i)=b_hat(ii,0);
                //std::cout << b_hat(ii,0) << "\t";
                ok=0;
                break;
            }
        }
        if(ok)
        {
            CZ(0,i)=0;
            //   std::cout << "0\t";
        }

    }
    update();
    //std::cout << std::endl;




    //Brak rozwiązań lub jedno rozwiązanie
    if(max<=0.001) //max z c_hat mniejsze od zera
    {
        update();
        qDebug () << "Mamy rozwiazanie" ;
        for(int i=0;i<m;i++)
        {
            //Sprawdzam czy zmienne sztuczne są w bazowych
            if(bv[i]>=n-artificial_var_count)
            {
                std::cout << "Brak rozwiązań" << std::endl;
                return 2;
            }
        }


        Eigen::MatrixXf As=AB.inverse()*A;
        //Eigen::MatrixXf Cs=-(C.transpose()-As.transpose()*CB.transpose());
        Eigen::MatrixXf Bs=AB.inverse()*B;


        std::vector<int> tmp_index;
        for(int i=0; i<n-m;i++)
        {
            std::cout << "nv:" << nv[i] << std::endl;
            if(std::abs(C_hat(i))<0.001)
            {
                tmp_index.push_back(nv[i]);
                std::cout << "wchodze" <<std::endl;
            }
        }

        qDebug() << "Znalzałem c_hat równe 0 szt: " << tmp_index.size();

        //Jezeli Cs dla niebazowych różne od zero to jedno rozwiazanie
        if(C_hat.maxCoeff()<0/*tmp_index.size()==0*/)
        {
            qDebug() << "Cs dla niebazowych różne od zero to jedno rozwiazanie" ;
            return 1;
        }
        if(z==0 && Bs.minCoeff()==0 && Bs.maxCoeff()==0)
        {
            qDebug() << "Rozwiazanie zdegenorowane";
            return 6;
        }


        std::cout << "z=" << z  << std::endl << "Bs=" <<std::endl << Bs << std::endl;
        if(Bs.minCoeff()>=0)
        {
            char tmp_text[256];
            char text_punkty[20];
            //Sprawdzam czy As(i) jest ujemne dla i:Ch=0
            if(As.col(tmp_index[0]).maxCoeff()<=0.001)
            {
                qDebug() << "Zbior nieograniczony!!!";
                textStatus="Wiele rozwiazan na zb. nieograniczony z poczatkiem w pkt:{";
                for(int i=0;i<n_w;i++)
                {
                    std::sprintf(text_punkty,"x%d=%.2f ",i+1,CZ(0,i));
                    textStatus+=text_punkty;
                }
                textStatus+="}";
                sprintf(tmp_text,"X={x:x=[%.2f, %.2f]^T+[%.2f, %.2f]^T*t,t>=0}",CZ(0),CZ(1),CZ(0),-CZ(0)*C(0)/C(1));
                longText=tmp_text;
                return 7;
            }
            textStatus="Wiele rozwiazan na zb. ograniczonym na pkt:{";

            for(int i=0;i<n_w;i++)
            {
                std::sprintf(text_punkty,"x%d=%.2f ",i+1,CZ(0,i));
                textStatus+=text_punkty;
            }
            textStatus+="}";

            std::vector<int> tmp_index_j;
            for(int i_index=0;i_index<tmp_index.size();i_index++)
            {
                qDebug() << "ilosc i znalezionych: " << tmp_index.size();
                std::cout << "A(i)=" << std::endl << As.col(tmp_index[0]) << std::endl;
                tmp_index_j.clear();
                for(int i=0;i<m;i++)
                {
                    if(As(i,tmp_index[i_index])>0 )
                    {
                        qDebug() << "AS >0 dla j=" << i ;
                        tmp_index_j.push_back(i);
                    }
                }

                if(tmp_index_j.size()==0)
                {
                    qDebug() << "Nie znalazłem j";
                    continue;
                    return 1;
                }
                int j_index=0;
                 float wsp2[10];
                // for(int j_index=0;j_index<tmp_index_j.size();j_index++)
                {
                    std::cout << "As:"<< std::endl;
                    std::cout <<As << std::endl <<std::endl ;
                    std::cout << "Cs:"<< std::endl;
                    //std::cout <<Cs << std::endl <<std::endl ;
                    std::cout << "Bs:"<< std::endl;
                    std::cout <<Bs << std::endl <<std::endl ;




                    //std::sort(tmp_index.begin(),tmp_index.end());
                    Eigen::MatrixXf AB_tmp=AB;
                    std::vector<int> tmp_bv;
                    tmp_bv=bv;
                    /*for(int tmp_i=0; tmp_i<tmp_bv.size();tmp_i++)  //Bo nie pamiętam czy i już było
                    {
                        if(tmp_bv[tmp_i]==tmp_index_j[j_index])
                        {
                            qDebug() << "zamieniam " << tmp_index[i_index] << " z " << tmp_index_j[j_index];
                            tmp_bv[tmp_i]=tmp_index[i_index];
                        }
                    }
                    */



                    /*for(int i=0;i<tmp_bv.size();i++)
                    {
                        AB_tmp.col(i) << A.col(tmp_bv[i]);
                    }
                    */
                    tmp_bv[tmp_index_j[0]]=tmp_index[0];

                    for(int i=0;i<tmp_bv.size();i++)
                    {
                        qDebug() << "BV[" << i << "]= " << tmp_bv[i];
                        AB_tmp.col(i) << A.col(tmp_bv[i]);
                    }
                    //for(int i=0;i<tmp_index_j.size();i++)
                    //{
                    //    AB_tmp.col(i+tmp_index.size()) << A.col(tmp_index_j[i]);
                    //}



                    Eigen::MatrixXf opt=AB_tmp.inverse()*B;

                    std::cout << "opt:" << std::endl<< opt << std::endl;
                    for(int i=0;i<bv.size();i++)
                    {
                        std::cout << "BV[" << i+1 <<"]:" << bv[i] << std::endl;
                    }
                    for(int i=0;i<tmp_index.size();i++)
                    {
                        std::cout << "i[" << i+1 <<"]:" << tmp_index[i]<< std::endl;
                    }
                    for(int i=0;i<tmp_index_j.size();i++)
                    {
                        std::cout << "j[" << i+1 <<"]:" << tmp_index_j[i]<< std::endl;
                    }


                    textStatus+=",{";
                    std::cout << "OPT: " <<std::endl << opt << std::endl;
                    for(int i=0;i<n_w;i++)
                    {
                        std::vector<int>::iterator it;

                        it = std::find (tmp_bv.begin(), tmp_bv.end(), i);
                        if (it != tmp_bv.end() )
                        {
                            std::sprintf(text_punkty,"x%d=%.2f ",i+1,(float)opt(it-tmp_bv.begin()));
                            wsp2[i]=(float)opt(it-tmp_bv.begin());
                        }else{
                            wsp2[i]=0;
                            std::sprintf(text_punkty,"x%d=%.2f ",i+1,(float)0);
                        }
                        textStatus+=text_punkty;
                    }
                    textStatus+="}";
                }
                sprintf(tmp_text,"X={x:x=((%.2f)*lambda+%.2f),((%.2f)*lambda+%.2f),lambda=[0,1]}",CZ(0)-wsp2[0],wsp2[0],CZ(1)-wsp2[1],wsp2[1]);
                longText=tmp_text;

            }
            std::cout << textStatus << std::endl;
            return 5;
        }
        return 1;
    }







    int j;

    //Warunek wejscia max z c_hat
    j=maxCol;

    //std::cout << "AB" << std::endl << AB << std::endl;

    Eigen::MatrixXf aj_hat=AB.inverse()*AI.col(j);

    if(aj_hat.maxCoeff(&maxRow, &maxCol)<0 )
    {
        std::cout << "Chat[j]:" << std::endl << C_hat(j) << std::endl << "j=" << j << std::endl;
        std::cout << "Problem nieograniczony:" << std::endl << "aj_hat" << std::endl << aj_hat << std::endl;
        qDebug() << "Problem nieograniczony!!!";
        return 3;
    }



    std::cout << "j=" << std::endl << j << std::endl;
    // std::cout << "AI.col(j)" << std::endl << AI.col(j) << std::endl;
    //  std::cout << "AI" << std::endl << AI << std::endl;
    //  std::cout << "CI" << std::endl <<  CI << std::endl;
    //  std::cout << "pi" << std::endl <<  pi << std::endl;
    std::cout << "c_hat" << std::endl <<  C_hat << std::endl;
    Eigen::MatrixXf As=AB.inverse()*A;
    Eigen::MatrixXf Cs=-(C.transpose()-As.transpose()*CB.transpose());
    Eigen::MatrixXf Bs=AB.inverse()*B;

    std::cout << "Cs:" << std::endl << Cs <<std::endl;

    //  std::cout << "b_hat" << std::endl <<  b_hat << std::endl;
    std::cout << "aj_hat" << std::endl <<  aj_hat << std::endl;




    //Sprawdzam czy jest to problem nieograniczony





    //Warunek wyjscia
    out_con = b_hat.array()/aj_hat.array();
    max=1;
    std::cout << "out_con" << std::endl <<  out_con << std::endl;


    std::vector<std::pair<int,int> > out_con2;
    out_con2.clear();
    for(int i=0;i<m;i++)
    {
        out_con2.push_back(std::pair<int,int>(bv[i],i));
    }

    //Sortuje po index
    std::sort(out_con2.begin(), out_con2.end(), sort_index);


    //Wyswietlam
    /* for(int i=0;i<out_con2.size();i++)
    {
        std::cout << out_con2[i].first << " - " << out_con2[i].second << "  " << out_con(out_con2[i].second,0) << std::endl;
    }*/

    max=10000;
    for(int i=0;i<out_con2.size();i++)
    {
        if(out_con(out_con2[i].second,0)>=0 && out_con(out_con2[i].second,0)<max && (aj_hat(out_con2[i].second)>=0))
        {
            max=out_con(out_con2[i].second,0);
            maxRow=out_con2[i].second;
        }
    }
    std::cout << "Wybieram " << maxRow << "=" <<out_con(maxRow) << std::endl;


    //Podmieniam jedna kolumne z bazowych na z niebazowych
    int tmp=bv[maxRow];
    bv[maxRow]=nv[j];
    nv[j]=tmp;

    for(int i=0;i<n-m;i++)
    {
        std::cout << "nv[" << i << "]: " << nv[i]+1 << std::endl;
    }
    std::cout << std::endl;

    //Wyswietlam wektor indeksów bazowych
    for(int i=0;i<m;i++)
    {
        std::cout << "bv[" << i << "]: " << bv[i]+1 << std::endl;
    }
    step++;
    update();
    return 0;

}

int TabelaSimplex::plot()
{

    //qDebug() << "m1=" << m1 << " m2=" <<m2 << " n_w" << n_w << " z=" << z;
    Eigen::MatrixXf A,B;

    A.resize(m1+m2,n_w);
    A << A1,A2;
    B.resize(m1+m2,1);
    B << B1 , B2;
    std::vector<point> pkt; //Wszystkie podejrzane punkty
    std::vector<point> pkt_ok; //Wszystkie punkty ktre sa ok

    pkt.push_back(point(0,0));
    double x1,x2;
    //szukam punktow ograniczen
    for(int i=0; i<m1+m2;i++)
    {
        //Szukam punktw wspolnych
        for(int j=i+1; j<m1+m2;j++)
        {
            if(A(i,1)!=0 && A(i,0)!=0 && A(j,1)!=0 && A(j,0)!=0)
            {
                x1=(B(i)/A(i,1)-B(j)/A(j,1)) /(A(i,0)/A(i,1)-A(j,0)/A(j,1));
            }
            else if(A(i,0)!=0)
            {
                x1=B(i)/A(i,0);
            }

            if(A(i,0)!=0 && A(i,1)!=0)
            {
                x2=(B(i)-A(i,0)*x1)/A(i,1);
            }
            else if(A(j,0)!=0 && A(j,1)!=0)
            {
                x2=(B(i)-A(j,0)*x1)/A(j,1);
            }else
            {
                x2=B(j)/A(j,1);
            }


            std::cout << "x1=" << x1 <<" x2=" << x2 << std::endl;
            pkt.push_back(point(x1,x2));
            //std::cout << "B1:" << B(i) << " B2:" << B(j) << std::endl;
            //std::cout << "A21:" << A(i,1) << " A22:" << A(j,1)<< std::endl<< std::endl;

        }
        //Punkty przeciecia sie z osia zer
        x1=B(i)/A(i,0);
        x2=0;
        pkt.push_back(point(x1,x2));
        std::cout << "x1=" << x1 <<" x2=" << x2 << std::endl;

        x1=0;
        x2=B(i)/A(i,1);
        pkt.push_back(point(x1,x2));


        x1=10000;
        x2=B(i)-A(i,0)*x1/A(i,1)-10;
        pkt.push_back(point(x1,x2));
        std::cout << "x1=" << x1 <<" x2=" << x2 << std::endl;

        //pkt.push_back(point(0,10000));



        //std::cout << "x1=" << x1 <<" x2=" << x2 << std::endl;
    }

    std::sort(pkt.begin(),pkt.end());

    pkt.push_back(point(7,0));

    //Odrzucam miepasujace
    bool ok;
    for(int i=0; i<pkt.size();i++)
    {
        ok=1;
        //Sprawdzam czy x1 i x2 wieksze lub rowne 0
        if((pkt[i].x<0) || (pkt[i].y<0))
            ok=0;

        for(int j1=0;j1<m1;j1++)
        {
            //sprawdzam czy spelnia warunki z macierzy A i B
            if((A1(j1,0)*pkt[i].x+A1(j1,1)*pkt[i].y)>B1(j1)+0.1)
                ok=0;
        }
        for(int j2=0;j2<m2;j2++)
        {
            //sprawdzam czy spelnia warunki z macierzy A i B
            if((A2(j2,0)*pkt[i].x+A2(j2,1)*pkt[i].y)<B2(j2)-0.1)
                ok=0;
        }

        if(ok==1)
        {
            std::cout << "x1=" << pkt[i].x <<" x2=" << pkt[i].y << std::endl;
            pkt_ok.push_back(pkt[i]);
        }
    }

    pkt.clear();
    //  pkt.push_back(pkt_ok[0]);
    //  pkt_ok.pop_back();
    ok=0;
    while(ok==0 &&pkt_ok.size()>0)
    {
        int tmp=check_intersection2(pkt_ok);
        if(tmp==-1)
            break;
        point tmp_point=pkt_ok[tmp];
        pkt_ok[tmp]=pkt_ok[pkt_ok.size()-1];
        pkt_ok[pkt_ok.size()-1]=tmp_point;
    }

    std::fstream plik;
    plik.open("tmp", std::ios_base::out);

    //Wyswietlam i zapisuje do pliku
    for(int i=0; i<pkt_ok.size(); i++)
    {
        plik << pkt_ok[i].x << "; " <<pkt_ok[i].y << ";" << std::endl;
        std::cout << "pkt: " << pkt_ok[i].x << " " << pkt_ok[i].y <<std::endl;
    }
    plik.close();
    char warstwice[1000];
    wykres.PrzeslijDoGNUPlota("set title \"Warstwica Simplex\"\n");
    wykres.PrzeslijDoGNUPlota("set style fill pattern 2\n");
    wykres.PrzeslijDoGNUPlota("set style data lines\n");
    wykres.PrzeslijDoGNUPlota("set pointsize 3\n");
    wykres.PrzeslijDoGNUPlota("set xrange [0:7]\n");
    wykres.PrzeslijDoGNUPlota("set yrange [0:7]\n");
    wykres.PrzeslijDoGNUPlota("set xlabel \"x1\"\n");
    wykres.PrzeslijDoGNUPlota("set ylabel \"x2\"\n");
    wykres.PrzeslijDoGNUPlota("unset key\n");
    wykres.PrzeslijDoGNUPlota("plot 'tmp' using 1:2 with filledcu ");

    std::sprintf(warstwice," (%F-%F*x)/%F ",z,C(0),C(1));
    std::replace( warstwice, warstwice+strlen(warstwice), ',', '.');
    wykres.PrzeslijDoGNUPlota(",");
    wykres.PrzeslijDoGNUPlota(warstwice);

    std::cout << "ilosc pkt: " << kolejne_rozwiazania.size() << std::endl;
    if(kolejne_rozwiazania.size() >0)
    {
        wykres.PrzeslijDoGNUPlota(",'-' w points pt 7 \n");
        std::cout << ",'-' w points pt 7 \n";
        for(int i=0; i<kolejne_rozwiazania.size();i++)
        {
            sprintf(warstwice," %F %F \n\0",kolejne_rozwiazania[i].x,kolejne_rozwiazania[i].y);
            std::replace(warstwice, warstwice+strlen(warstwice), ',', '.');
            std::cout << warstwice;
            wykres.PrzeslijDoGNUPlota(warstwice);
        }
        std::cout << "e\n" <<std::endl;
        wykres.PrzeslijDoGNUPlota("e\n");

    }
    wykres.PrzeslijDoGNUPlota("\n");
    wykres.PrzeslijDoGNUPlota("replot\n");
}

TabelaSimplex::TabelaSimplex()
{
    wykres.init();
}

TabelaSimplex::TabelaSimplex(QWidget *parent):QTableWidget(parent)
{
    wykres.init();
}

TabelaSimplex::~TabelaSimplex()
{

}

void TabelaSimplex::SetMatix(Eigen::MatrixXf A1, Eigen::MatrixXf A2, Eigen::MatrixXf B1, Eigen::MatrixXf B2, Eigen::MatrixXf C)
{
    kolejne_rozwiazania.clear();
    this->A1=A1;
    this->A2=A2;
    this->B1=B1;
    this->B2=B2;
    m1=A1.rows();
    m2=A2.rows();
    n_w=C.cols();

    QFont bold;
    bold.setBold(1);
    step=0;

    CiCx=new QTableWidgetItem("Ci/Cx");
    CiCx->setTextAlignment(Qt::AlignCenter);
    CiCx->setFont(bold);

    XiXj=new QTableWidgetItem("Xi/Xj");
    XiXj->setTextAlignment(Qt::AlignCenter);
    XiXj->setFont(bold);

    Bi_name=new QTableWidgetItem("Bi");
    Bi_name->setTextAlignment(Qt::AlignCenter);
    Bi_name->setFont(bold);

    BiAk=new QTableWidgetItem("---");
    BiAk->setTextAlignment(Qt::AlignCenter);
    BiAk->setFont(bold);

    dash1=new QTableWidgetItem("-");
    dash1->setTextAlignment(Qt::AlignCenter);

    dash2=new QTableWidgetItem("-");
    dash2->setTextAlignment(Qt::AlignCenter);

    ZjCj_name=new QTableWidgetItem("Opt ster:");
    ZjCj_name->setTextAlignment(Qt::AlignCenter);

    Zj_name=new QTableWidgetItem("---");
    Zj_name->setTextAlignment(Qt::AlignCenter);

    Zx=new QTableWidgetItem("Z(x)=");
    Zx->setTextAlignment(Qt::AlignCenter);
    Zx->setFont(bold);

    A_matrix.clear();
    Xi_names.clear();
    Xj_names.clear();
    Bi.clear();
    BiAi.clear();

    Cj.clear();
    Ci.clear();
    ZjCj.clear();
    Zj.clear();

    bv.clear();
    nv.clear();

    //Tworze Podstawową macierz A
    A.resize(A1.rows()+A2.rows(),A1.cols()+A1.rows()+A2.rows()+A2.rows());
    A.block(0,0,A1.rows()+A2.rows(),A2.cols()) << A1, A2;
    //Dodaje zmienne dodatkowe
    A.bottomRightCorner(A1.rows()+A2.rows(),A1.rows()+A2.rows()+A2.rows()).setIdentity();
    //Zmieniam znak przy zmiennych dodatkowych dla warunku większościowego (A2)
    A.bottomRightCorner(A2.rows(),A2.rows()+A2.rows())=A.bottomRightCorner(A2.rows(),A2.rows()+A2.rows())*-1;
    //Dodaje jedynki dla zmiennych sztucznych
    A.bottomRightCorner(A2.rows(),A2.rows()).setIdentity();
    qDebug() << "test1" ;
    //Tworze podstawowy vektor B
    B.resize(B1.rows()+B2.rows(),1);
    B << B1, B2;
    //Tworze podstawowy wektor C
    this->C.resize(1,A1.cols()+A1.rows()+A2.rows()+A2.rows());
    this->C.block(0,0,C.rows(),C.cols()) << C;
    //Dodaje zera do vektora C dla zmiennych dodatkowych
    this->C.bottomRightCorner(1,A1.rows()+A2.rows()+A2.rows()).setZero();
    this->C.bottomRightCorner(1,A2.rows()).setConstant(-10000);

    //Ustawiam wymiar pi out_con C_hat
    //pi.resize(1,);
    //C_hat;
    //out_con;


    artificial_var_count=A2.rows();
    n=this->C.cols();
    m=this->B.rows();

    BA.setZero(m,1);
    Z.setZero(1,n);
    CZ.setZero(1,n);

    qDebug() << "m: " << m;
    qDebug() << "n: " << n;

    this->setColumnCount(1);
    this->setRowCount(1);
    this->setColumnCount(n+4);
    this->setRowCount(m+4);


    //Wartości domyślne dla bv
    //Dla macierzy A1
    for(int i=0;i<m-A2.rows();i++)
    {
        bv.push_back(i+std::max(A1.cols(),A2.cols()));
        qDebug() << "aaa";
    }
    //Dla macierzy A2
    for(int i=n-A2.rows();i<n;i++)
    {
        bv.push_back(i);
        qDebug() << "bbb";
    }

    //i dla nv
    bool ok=1;
    for(int i=0;i<n;i++)
    {
        ok=1;
        for(int j=0;j<m;j++)
        {
            if(bv[j]==i)
                ok=0;
        }
        if(ok)
        {
            nv.push_back(i);
            qDebug() << i;
        }
    }

    qDebug() << "test3" ;

    //Tworzę nazwy Xi i Ci - pionowo
    for(int i=0;i<m;i++)
    {
        QTableWidgetItem* tmp=new QTableWidgetItem(QString("x"));
        tmp->setFont(bold);
        tmp->setTextAlignment(Qt::AlignCenter);
        Xi_names.push_back(tmp);

        tmp=new QTableWidgetItem(QString("x"));
        tmp->setTextAlignment(Qt::AlignCenter);
        Ci.push_back(tmp);

        tmp=new QTableWidgetItem(QString("x"));
        tmp->setTextAlignment(Qt::AlignCenter);
        Bi.push_back(tmp);

        tmp=new QTableWidgetItem(QString("BiAi"));
        tmp->setTextAlignment(Qt::AlignCenter);
        BiAi.push_back(tmp);


    }


    //Tworzę nazwy Xj i Xj - poziomo
    for(int i=0; i<n;i++)
    {
        QTableWidgetItem* tmp;
        if(i<n-A2.rows())
            tmp=new QTableWidgetItem(QString("x%1").arg(i+1));
        else
            tmp=new QTableWidgetItem(QString("s%1").arg(i-n+A2.rows()+1));

        tmp->setFont(bold);
        tmp->setTextAlignment(Qt::AlignCenter);
        Xj_names.push_back(tmp);

        tmp=new QTableWidgetItem(QString(""));
        tmp->setFont(bold);
        tmp->setTextAlignment(Qt::AlignCenter);
        Cj.push_back(tmp);

        tmp=new QTableWidgetItem(QString("Zj"));
        tmp->setTextAlignment(Qt::AlignCenter);
        Zj.push_back(tmp);

        tmp=new QTableWidgetItem(QString("ZjCj"));
        tmp->setTextAlignment(Qt::AlignCenter);
        ZjCj.push_back(tmp);
    }

    //Tworzę macierz A
    for(int i=0;i<m;i++)
    {
        std::vector<QTableWidgetItem*> tmpvector;
        for(int j=0; j<n; j++)
        {
            QTableWidgetItem* tmp=new QTableWidgetItem(QString("x"));
            tmp=new QTableWidgetItem(QString("x"));
            tmp->setTextAlignment(Qt::AlignCenter);
            tmpvector.push_back(tmp);
        }
        A_matrix.push_back(tmpvector);
    }
    first_bv=bv;
    build_table();
    update();
    z=0;
}

void TabelaSimplex::build_table()
{
    this->reset();
    //Pierwsza linia
    this->setItem(0,0,CiCx);
    this->setItem(0,1,dash1);

    qDebug() << "test11";
    for(int i=0;i<n;i++)
    {
        this->setItem(0,2+i,Cj[i]);
        this->setItem(m+2,2+i,Zj[i]);
        this->setItem(m+3,2+i,ZjCj[i]);
    }
    this->setItem(0,n+2,Bi_name);
    this->setItem(0,n+3,BiAk);
    this->setSpan(0,n+2,2,1);
    this->setSpan(0,n+3,2,1);

    //Druga linia
    this->setItem(1,0,dash2);
    this->setItem(1,1,XiXj);
    for(int i=0;i<n;i++)
    {
        this->setItem(1,2+i,Xj_names[i]);
    }
    //Kolejne linie
    //Ci Xi

    for(int i=0;i<m;i++)
    {
        this->setItem(2+i,0,Ci[i]);
        this->setItem(2+i,1,Xi_names[i]);
        this->setItem(2+i,n+2,Bi[i]);
        this->setItem(2+i,n+3,BiAi[i]);

    }

    //Macierz A
    for(int i=0;i<m;i++)
    {
        for(int j=0; j<n; j++)
        {
            this->setItem(2+i,2+j,A_matrix[i][j]);
        }
    }
    this->setItem(m+2,0,Zj_name);
    this->setSpan(m+2,0,1,2);

    this->setItem(m+3,0,ZjCj_name);
    this->setSpan(m+3,0,1,2);

    this->setItem(m+2,n+2,Zx);
    this->setSpan(m+2,n+2,2,2);



}

void TabelaSimplex::update()
{
    kolejne_rozwiazania.push_back(point(CZ(0,0),CZ(0,1)));
    qDebug() << "Z1=" << CZ(0,0) << " Z2=" << CZ(0,1);
    QFont bold;
    bold.setBold(1);
    //Aktualizuje Cj Poziomo
    for(int i=0; i<n;i++)
    {
        Cj[i]->setText(QString("%1").arg(C(0,i)));
        Zj[i]->setText(QString("%1").arg(Z(0,i)));
        ZjCj[i]->setText(QString("%1").arg(CZ(0,i)));

    }
    //Aktualizuje Ci Pionowo
    for(int i=0; i<m;i++)
    {
        Xi_names[i]->setText(Xj_names[bv[i]]->text());
        Ci[i]->setText(Cj[bv[i]]->text());
    }

    //Aktualizuje Bi Pionowo
    for(int i=0; i<m;i++)
    {
        Bi[i]->setText(QString("%1").arg(B(i,0)));
        BiAi[i]->setText(QString("%1").arg(BA(i,0)));
    }

    //Aktualizuje macierz A
    for(int i=0;i<m;i++)
    {
        for(int j=0; j<n; j++)
        {
            A_matrix[i][j]->setText(QString("%1").arg(A(i,j)));
        }
    }


}

