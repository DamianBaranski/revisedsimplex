#include <gnuplot.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <cstdio>
#include <iostream>
#include <string.h>

#define STDIN        0
#define STDOUT       1


using namespace std; 

 bool gnuplot::PrzeslijDoGNUPlota(const char *Komunikat)
 {
  int Ilosc = strlen(Komunikat);
  int IloscPrzeslanych;

  while (((IloscPrzeslanych = write(_Wejscie_GNUPlota,Komunikat,Ilosc)) != -1)
          && Ilosc > 0) {
    Komunikat += IloscPrzeslanych;
    Ilosc -= IloscPrzeslanych;
  }
  if (Ilosc > 0) {
    printf("!!! Przeslanie polecenia do GNUPlota nie powiodlo sie.");
    return false;
  }
  return true;
 }



int gnuplot::init()
 {
  int indesk[2],outdesk[2];

  if (pipe(indesk) == -1 || pipe(outdesk) == -1) {
    printf(
      "### Niemozliwe utworzenie kanalow komunikacyjnych do podprocesow.");
    return false;
  }

  switch (fork()) {
    case -1: printf("### Niemozliwe rozwidlenie procesu.");
             return false;
    case  0: 

       if (close(STDIN) == -1) { 
         printf("### Blad przy zamykaniu strumienia wejsciowego.");
         return false;
       }
       if (dup(outdesk[STDIN]) == -1) {
         printf("### Blad duplikacji kanalu wejsciowego.");  
         return false;
       }
       
       if (close(STDOUT) == -1) { 
         printf(
            "### Blad zamkniecia kanalu standardowego kanalu wyjsciowego."); 
         return false;
       }
       if (dup(indesk[STDOUT]) == -1) {
         printf("### Blad duplikacji kanalu wyjsciowego.");  
         return false;
       }       
       if (close(outdesk[STDIN]) == -1) {
         printf("### Blad zamkniecia kanalu wejsciowego.");
         return false;
       }
       if (close(outdesk[STDOUT]) == -1) {
         printf("### Blad zamkniecia kanalu wyjsciowego.");
         return false;
       }
       if (close(indesk[STDIN]) == -1) {
         printf("### Blad zamkniecia duplikatu kanalu wejsciowego.");
         return false;
       }
       if (close(indesk[STDOUT]) == -1) {
         printf("### Blad zamkniecia duplikatu kanalu wyjsciowego.");
         return false;
       }
       char Tab[3];
       read(STDIN,Tab,1);
       write(STDOUT,"\n",1); 
       const char *Comm;

       execlp(Comm = "gnuplot","gnuplot",NULL);

       printf("!!! Blad:"); 
       printf("!!! W procesie potomnym nie mogl zostac."); 
       printf("!!! uruchomiony program gnuplot."); 
       printf("!!! Nastapilo przerwanie dzialania procesu potomnego."); 
       printf("!!! Jednym z mozliwych powodow problemu moze byc"); 
       printf("!!! Brak programu gnuplot w szukanej sciezce."); 
       printf("!!! Do uruchomienia programu gnuplot bylo uzyte polecenie:");
       printf(Comm);
       return false;

    default:
       if (close(outdesk[STDIN]) == -1 || close(indesk[STDOUT]) == -1) { 
         printf(" Blad zamkniecia outpipe[STDIN], inpipe[STDOUT]."); 
       }

       fcntl(indesk[STDIN],F_SETFL,O_NDELAY);
       _Wyjscie_GNUPlota  = indesk[STDIN];
       _Wejscie_GNUPlota =  outdesk[STDOUT];
       PrzeslijDoGNUPlota("\n\n\nset output \nset term x11\n");

  }

}

/*


	PrzeslijDoGNUPlota("\n\n\nset output \nset term x11\n");
	PrzeslijDoGNUPlota("\n");

	PrzeslijDoGNUPlota("plot 2\n");


   cin.get();
PrzeslijDoGNUPlota("plot 3\n");
   cin.get();

   
   cout << "wszystko ok" << endl;
 }

*/
