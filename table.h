#ifndef TABLE_H
#define TABLE_H

#include <QWidget>
#include <QObject>
#include <QTableView>
#include <eigen3/Eigen/Eigen>
#include <QStandardItemModel>
#include <string>
#include <fstream>

class table : public QTableView
{
private:
    Eigen::MatrixXf *Matrix;
    int n;
    int m;
    QStandardItemModel model;

public:
    table();
    table(QWidget *parent=0);
    void setMatrix(Eigen::MatrixXf *matrix);
    void update();
    std::string save(std::fstream *file);
    void open(std::fstream *file, int rows, int cols);
    ~table();
    void closeEditor( QWidget * editor, QAbstractItemDelegate::EndEditHint hint );

};

#endif // TABLE_H
